package com.example.recycleview

import ItemAdapter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val itemList = mutableListOf<String>()
    private lateinit var adapter: ItemAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ItemAdapter(itemList)
        binding.recyclerView.adapter = adapter

        binding.addButton.setOnClickListener {
            val newItem = binding.editText.text.toString()
            if (newItem.isNotEmpty()) {
                itemList.add(newItem)
                adapter.notifyItemInserted(itemList.size - 1)
                binding.editText.text.clear()
            }
        }
    }
}